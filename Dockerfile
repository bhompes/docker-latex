FROM blang/latex:ctanfull
LABEL org.opencontainers.image.authors="bart.hompes@artifexconsultancy.nl"

# Install GIT and remove apt cache
RUN apt-get update \
     && apt-get install -qy git \
     && apt-get install -qy ghostscript \
	 && rm -rf /var/lib/apt/lists/*