# Docker image
by B.F.A. Hompes [bart.hompes@artifexconsultancy.nl](mailto:bart.hompes@artifexconsultancy.nl)

Docker image for latex compilation.

## Versions
- [bhompes/latex:latest](https://hub.docker.com/r/bhompes/latex/)
  - based on [blang/latex:ctanfull](https://github.com/blang/latex-docker) which contains Ubuntu with CTAN TexLive Scheme-full: up-to-date, all packages
  - Includes GhostScript (for epstopdf)
  - Includes GIT
  
## How to build
- Update Dockerfile
- Run "docker build -t bhompes/latex ."
- Run "docker login" and enter credentials to docker hub
- Run "docker push bhompes/latex"